(define-module (lilv)
  #:use-module (lilv oop base)
  #:use-module (lilv oop plugin)
  #:use-module (lilv oop port)
  #:use-module (lilv oop preset)
  #:use-module (lilv oop world)
  #:use-module (lilv oop world model)
  #:use-module (lilv oop node)
  #:use-module (lilv oop collection)
  #:use-module (oop goops)
  #:re-export (plugin
               ->list
               value
               uri
               name
               presets
               world-get
               world-find-nodes
               world-ask
               world-plugins
               port
               port-symbol
               port-index
               ports
               port-supports?)
  #:export (lilv-init))

(define (lilv-init)
  (world-load-all))

