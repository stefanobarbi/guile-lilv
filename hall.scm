(hall-description
  (name "lilv")
  (prefix "guile")
  (version "0.1")
  (author "Stefano Barbi")
  (copyright (2019))
  (synopsis "guile bindings to lilv library")
  (description "")
  (home-page
    "https://gitlab.com/stefanobarbi/guile-lilv")
  (license gpl3+)
  (dependencies `())
  (files (libraries
           ((scheme-file "lilv")
            (directory
              "lilv"
              ((directory
                 "oop"
                 ((directory "world" ((scheme-file "model")))
                  (scheme-file "port")
                  (scheme-file "node")
                  (scheme-file "plugin")
                  (scheme-file "collection")
                  (scheme-file "world")
                  (scheme-file "lv2core")
                  (scheme-file "preset")
                  (scheme-file "base")))))))
         (tests ((directory
                   "tests"
                   ((directory "oop" ((scheme-file "base")))))))
         (programs ((directory "scripts" ())))
         (documentation
           ((org-file "README")
            (symlink "README" "README.org")
            (text-file "HACKING")
            (text-file "COPYING")
            (directory "doc" ((texi-file "lilv")))))
         (infrastructure
           ((scheme-file "guix") (scheme-file "hall")))))

