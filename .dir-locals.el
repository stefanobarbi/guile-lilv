((scheme-mode  . ((geiser-scheme-implementation . guile)
                  (geiser-guile-extra-keywords
                   "define-lilv"
                   "lilv-bind"
                   "define-lilv-method"
                   "defile-lilv-collection"))))
