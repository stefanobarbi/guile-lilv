(define-module (lilv oop preset)
  #:use-module (lilv oop base)
  #:use-module (lilv oop world model)
  #:use-module (lilv oop node)
  #:use-module (lilv oop plugin)
  #:use-module (lilv oop collection)
  #:use-module (oop goops)
  #:export (<preset>
            presets))



(define-class <preset> ()
  (uri    #:init-keyword #:uri)
  (plugin #:init-keyword #:plugin #:accessor plugin))


(define-method (uri (p <preset>))
  (slot-ref p 'uri))

(define-method (presets (p <plugin>))
  (map (lambda (uri)
         (make <preset> #:uri uri #:plugin p))
       (filter
        (lambda (node)
          (world-ask `(,node a (uri "http://lv2plug.in/ns/ext/presets#Preset"))))
        (->list (world-find-nodes `(? (lv2 appliesTo) ,(uri p)))))))


(define-method (name (p <preset>))
  (world-get `(,(uri p) label ?)))
