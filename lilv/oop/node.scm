(define-module (lilv oop node)
  #:use-module (lilv oop base)
  #:use-module (lilv oop world)
  #:use-module (lilv oop collection)
  #:use-module (oop goops)
  #:use-module (ice-9 match)
  #:use-module ((system foreign) #:prefix ffi:)
  #:export (node
            value
            turtle
            <node>
            <node-uri>
            <node-blank>
            <node-bool>
            <node-float>
            <node-int>
            <node-string>
            <node-literal>
            <nodes>
            node-null?))


(define-generic value)
(define-generic turtle)

(lilv-bind
 ((free "lilv_node_free"))
 (define-class <node> (<lilv>)
   (free  #:init-value free
          #:allocation #:class)))

(define-class <node-uri>     (<node>))
(define-class <node-blank>   (<node>))
(define-class <node-bool>    (<node>))
(define-class <node-float>   (<node>))
(define-class <node-int>     (<node>))
(define-class <node-string>  (<node>))
(define-class <node-literal> (<node>))

(define-lilv-method (initialize (n <node>) initargs)
  ((%node-uri?	   (ffi:int "lilv_node_is_uri" '*))
   (%node-blank?   (ffi:int "lilv_node_is_blank" '*))
   (%node-bool?	   (ffi:int "lilv_node_is_bool" '*))
   (%node-float?   (ffi:int "lilv_node_is_float" '*))
   (%node-int?	   (ffi:int "lilv_node_is_int" '*))
   (%node-literal? (ffi:int "lilv_node_is_literal" '*))
   (%node-string?  (ffi:int "lilv_node_is_string" '*)))
  (next-method)
  (when (eq? (class-of n) <node>)
    (let [(pointer (slot-ref n 'pointer))]
     (cond
      ((positive? (%node-uri? pointer))     (change-class n <node-uri>     ))
      ((positive? (%node-blank? pointer))   (change-class n <node-blank>   ))
      ((positive? (%node-bool? pointer))    (change-class n <node-bool>    ))
      ((positive? (%node-float? pointer))   (change-class n <node-float>   ))
      ((positive? (%node-int? pointer))     (change-class n <node-int>     ))
      ((positive? (%node-string? pointer))  (change-class n <node-string>  ))
      ((positive? (%node-literal? pointer)) (change-class n <node-literal> ))))))


(define-lilv (%node class world args)
  ((lilv_new_uri      ('* "lilv_new_uri"      '* '*))
   (lilv_new_file_uri ('* "lilv_new_file_uri" '* '* '*))
   (lilv_new_string   ('* "lilv_new_string"   '* '*))
   (lilv_new_int      ('* "lilv_new_int"      '*  ffi:int))
   (lilv_new_float    ('* "lilv_new_float"    '*  ffi:float))
   (lilv_new_bool     ('* "lilv_new_bool"     '*  ffi:int)))
  
  (cond
   [(eq? class <node-uri>)
    (match args
      [((? string? uri))
       (make <node-uri>
         #:pointer (lilv_new_uri world (ffi:string->pointer uri)))]
      
      [(((? string? host) . (? string? path)))
       (make <node-uri>
         #:pointer (lilv_new_file_uri world (ffi:string->pointer host) (ffi:string->pointer path)))]

      [_ (goops-error "an uri must be a string or a pair (host . path)" args)])]

   [(eq? class <node-string>)
    (match args
      [((? string? str))
       (make <node-string>
         #:pointer (lilv_new_string world (ffi:string->pointer str)))]

      [_ (goops-error "a string argument must be provided" args)])]
   

   [(eq? class <node-int>)
    (match args
      [((? integer? n))
       (make <node-int>
         #:pointer (lilv_new_int world (inexact->exact n)))]

      [_ (goops-error "an integer argment must be supplied" args)])]
   

   [(eq? class <node-float>)
    (match args
      [((? number? n))
       (make <node-float>
         #:pointer (lilv_new_float world (exact->inexact n)))]

      [_ (goops-error "a float argument must be supplied" args)])]

   [(eq? class <node-bool>)
    (match args
      [((? boolean? n))
       (make <node-bool>
         #:pointer (lilv_new_bool world world (if n 1 0)))]

      [_ (goops-error "a boolean argument must be supplied" args)])]))


(define-method (node (c  <class>) . args)
  (%node c (->pointer (the-world)) args))


(define-lilv-method (value (n <node-uri>))
  (( %value ( '*   "lilv_node_as_uri" '* )))
  (ffi:pointer->string (%value (->pointer n))))

(define-lilv-method (value (n <node-blank>))
  ((%value( '*   "lilv_node_as_blank" '* )))
  (ffi:pointer->string (%value (->pointer n))))

(define-lilv-method (value (n <node-string>))
  ((%value( '*   "lilv_node_as_string" '* )))
  (ffi:pointer->string (%value (->pointer n))))

(define-lilv-method (value (n <node-float>))
  ((%value (  ffi:float  "lilv_node_as_float" '* )))
  (%value (->pointer n)))

(define-lilv-method (value (n <node-int>))
  ((%value(  ffi:int  "lilv_node_as_int" '* )))
  (%value (->pointer n)))

(define-lilv-method (value (n <node-bool>))
  ((%value(  ffi:int  "lilv_node_as_bool" '* )))
  (= 1 (%value (->pointer n))))

(define-lilv-method (equal? (a <node>) (b <node>))
  ((%equal? ( ffi:int "lilv_node_equals" '* '*)))
  (%equal? (->pointer a) (->pointer b)))

(define-lilv-collection <nodes>
  (on <node>)
  (next   "lilv_nodes_next")
  (start  "lilv_nodes_begin")
  (get    "lilv_nodes_get")
  (ended? "lilv_nodes_is_end")
  (size   "lilv_nodes_size"))

(lilv-bind
 ((free "lilv_nodes_free"))
 (class-slot-set!  <nodes>  'free free))

(define-lilv-method (turtle (n <node>))
  ((turtle ('* "lilv_node_get_turtle_token" '*)))
  (ffi:pointer->string (turtle (->pointer n))))


(define-method (uri (s <string>))
  (node <node-uri> s))

(define (node-null? n)
  (ffi:null-pointer? (->pointer n)))


