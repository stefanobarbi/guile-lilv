(define-module (lilv oop world model)
  #:use-module (ice-9 match)
  #:use-module ((system foreign) #:prefix ffi:)
  #:use-module (lilv oop world)
  #:use-module (lilv oop node)
  #:use-module (lilv oop base)
  #:use-module (lilv oop collection)
  #:use-module (oop goops)
  #:export (world-ask
            world-get
            world-find-nodes))

(define wildcard (make <node> #:pointer (ffi:make-pointer 0)))

(define-lilv-method (world-find-nodes (s <node>) (p <node>) (o <node>))
  ((find ('* "lilv_world_find_nodes" '* '* '* '*)))

  (let [(res (find (->pointer (the-world))
                   (->pointer s)
                   (->pointer p)
                   (->pointer o)))]
    (if (ffi:null-pointer? res)
        '()
        (make <nodes> #:pointer res))))


(define-lilv-method (world-ask (s <node>) (p <node>) (o <node>))
  ((ask (ffi:int "lilv_world_ask" '* '* '* '*)))

  (let [(res (ask (->pointer (the-world))
                  (->pointer s)
                  (->pointer p)
                  (->pointer o)))]
    (= 1 res)))


(define-lilv-method (world-get (s <node>) (p <node>) (o <node>))
  ((get ('* "lilv_world_get" '* '* '* '*)))
  (let [(ptr (get (->pointer (the-world))
                  (->pointer s)
                  (->pointer p)
                  (->pointer o)))]
    (if (ffi:null-pointer? ptr)
        #f
        (make <node> #:pointer ptr))))



(define rdf:type   (node <node-uri> "http://www.w3.org/1999/02/22-rdf-syntax-ns#type"))
(define rdfs:label (node <node-uri> "http://www.w3.org/2000/01/rdf-schema#label"))
(define doap:name  (node <node-uri> "http://usefulinc.com/ns/doap#name"))

(define (parse-node n)
  (if (is-a? n <node>)
      n
      (match n
        [('uri string)
         (node <node-uri> string)]

        ['a rdf:type]

        ['label rdfs:label]
        
        ['name doap:name]

        [(lv2 name)
         (module-ref (resolve-module '(lilv oop lv2core))
                     (string->symbol
                      (string-append "lv2core:"
                                     (symbol->string name))))]

        ['? wildcard]

        [(? string? s)
         (node <node-string> s)]

        [(? boolean? b)
         (node <node-bool> b)]

        [(and (? number? _) (? inexact? i))
         (node <node-float> i)]

        [(and (? number? _) (? exact-integer? i))
         (node <node-int> i)]

        [('int (? integer? i))
         (node <node-int> (inexact->exact i))]

        [('float (? number? i))
         (node <node-float> (exact->inexact i))])))


(define (parse-triple t)
  (match t
    [(s p o)
     (map parse-node t)]

    [_ (throw 'lilv-model-exception "not a triple")]))


(define-method (world-get (triple <list>))
  (apply world-get (parse-triple triple )))

(define-method (world-find-nodes (triple <list>))
  (apply world-find-nodes (parse-triple triple )))

(define-method (world-ask (triple <list>))
  (apply world-ask (parse-triple triple )))

