(define-module (lilv oop base)
  #:use-module ((system foreign) #:prefix ffi:)
  #:use-module (oop goops)
  #:export (->pointer
            pointer->
            <lilv>
            <lilv-meta>
            define-lilv-method
            define-lilv
            lilv-bind
            name
            uri))

(define %lilv (dynamic-link "liblilv-0"))


(define-generic ->pointer)
(define-generic pointer->)
(define-generic name)
(define-generic uri)

(define object-cache (make-weak-key-hash-table))

(define-class <lilv-meta> (<class>))

(define-method (make (o <lilv-meta>) . initargs)
  ;; Try to retrieve an object from the object cache instead of
  ;; instantiating a new one. sets the pointer finalizer and handles
  ;; common errors.
  (let [(ptr      (get-keyword #:pointer initargs #f))
        (collect? (get-keyword #:collect? initargs #t))]
    (if (and ptr (ffi:pointer? ptr))
        (or (hashv-ref object-cache (ffi:pointer-address ptr))
           (let ((instance (allocate-instance o initargs)))
             (initialize instance initargs)
             (hashv-set! object-cache (ffi:pointer-address ptr) instance)
             (when collect?
               (ffi:set-pointer-finalizer! ptr (class-slot-ref o 'free )))
             instance))
        (throw 'lilv-error "A valid pointer must be provided"))))


(define-class <lilv> ()
  (pointer  #:getter ->pointer
            #:init-keyword #:pointer)
  (free     #:init-value (dynamic-pointer "lilv_free" %lilv) #:allocation #:class)
  #:metaclass <lilv-meta>)

(define-syntax %ffi-bind-element
  (syntax-rules ()
    [(_ (ret cfun-name carg* ...))
     (ffi:pointer->procedure
      ret
      (dynamic-func cfun-name %lilv)
      (list carg* ...))]

    [(_ pointer)
     (dynamic-pointer pointer %lilv)]))

(define-syntax lilv-bind
  (syntax-rules ()
    [(_ ((bindings ffi-element) ...)
        body ...)
     (let ((bindings (%ffi-bind-element ffi-element)) ...)
       body ... )]))

(define-syntax define-lilv-method
  (syntax-rules ()
    [(_  (name formal* ... ) bindings body ...)
     (define-method (name formal* ...)
       (lilv-bind bindings body ...))]))

(define-syntax define-lilv
  (syntax-rules ()
    [(_  (name formal* ... ) bindings body ...)
     (define* (name formal* ...)
       (lilv-bind bindings body ...))]))

