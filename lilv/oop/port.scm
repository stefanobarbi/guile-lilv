(define-module (lilv oop port)
  #:use-module ((system foreign) #:prefix ffi:)
  #:use-module (lilv oop base)
  #:use-module (lilv oop world)
  #:use-module (lilv oop world model)
  #:use-module (lilv oop node)
  #:use-module (lilv oop collection)
  #:use-module (lilv oop plugin)
  #:use-module (rnrs bytevectors)
  #:use-module (srfi srfi-1)
  #:use-module (oop goops)
  #:export (ports
            port
            port-symbol
            port-index
            port-supports?
            port-range))

(define-class <port> (<lilv>)
  (plugin #:init-keyword #:plugin))

(define-method (plugin (p <port>))
  (slot-ref p 'plugin))


(define-lilv-method (port (p <plugin>) (index <integer>))
  ((get-port ('* "lilv_plugin_get_port_by_index" '* ffi:int)))
  (let [(ptr (get-port (->pointer p) index))]
    (if (ffi:null-pointer? ptr)
        (throw 'lilv-error "cannot get port i for plugin" index p)
        (make <port> #:pointer ptr #:plugin p #:collect? #f))))


(define-lilv-method (port (p <plugin>) (symbol <string>))
  ((get-port ('* "lilv_plugin_get_port_by_symbol" '* '*)))
  (let* [(sym (node <node-string> symbol))
         (ptr (get-port (->pointer p) (->pointer sym)))]
    (if (ffi:null-pointer? ptr)
        (throw 'lilv-error "cannot get port with symbol ~S for plugin" symbol p)
        (make <port> #:pointer ptr #:plugin p #:collect? #f))))

(define-lilv-method (name (port <port>))
  ((get-name ('* "lilv_port_get_name" '* '*)))
  (make <node>
    #:pointer (get-name (->pointer (plugin port)) (->pointer port))
    #:collect? #t))

(define-lilv-method (ports (plugin <plugin>))
  ((get_num_ports (ffi:uint32 "lilv_plugin_get_num_ports" '*)))
  (let [(n (get_num_ports (->pointer plugin)))]
    (unfold
     (lambda (i) (= i n))
     (lambda (i) (port plugin i))
     (lambda (i) (1+ i))
     0)))


(define-lilv-method (node (port <port>))
  ((get-node ('* "lilv_port_get_node" '* '*)))
  (make <node>
    #:pointer (get-node (->pointer (plugin port)) (->pointer port))
    #:collect? #f))

(define-lilv-method (port-symbol (port <port>))
  ((get-symbol ('* "lilv_port_get_symbol" '* '*)))
  (make <node>
    #:pointer (get-symbol (->pointer (plugin port)) (->pointer port))
    #:collect? #f))


(define-lilv-method (port-index (port <port>))
  ((get-index (ffi:uint32 "lilv_port_get_index" '* '*)))
  (get-index (->pointer (plugin port)) (->pointer port)))


(define-lilv-method (get (port <port>) (predicate <node>))
  ((get ('* "lilv_port_get" '* '* '*)))
  (make <node> #:pointer (get (->pointer (plugin port))
                              (->pointer port)
                              (->pointer predicate))))


(define-lilv-method (port-supports? (port <port>) (event-type <node>))
  ((supports? ('* "lilv_port_supports_event" '* '*)))
  (= (supports? (->pointer (plugin port))
                (->pointer port)
                (->pointer event-type))
     1))


(define-lilv-method (port-range (p <port>))
  ((get-range (ffi:void "lilv_port_get_range" '* '* '* '* '*)))
  (case (ffi:sizeof '*)
    [(8)
     (let [(ptrs (make-u64vector 3 0))]
       (get-range (->pointer (plugin p))
                  (->pointer p)
                  (ffi:bytevector->pointer ptrs 0)
                  (ffi:bytevector->pointer ptrs 8)
                  (ffi:bytevector->pointer ptrs 16))
       (map (lambda (addr)
              (make <node> #:pointer (ffi:make-pointer addr)))
            (u64vector->list ptrs)))]))

(define-class <scale-point> (<lilv>))



;; (define-lilv-collection <scale-points>
;;   (on <scale-point>)
;;   (next    "lilv_scale_points_next")
;;   (start   "lilv_scale_points_begin")
;;   (get     "lilv_scale_points_get")
;;   (ended?  "lilv_scale_points_is_end")
;;   (size    "lilv_scale_points_size"))

;; (lilv-bind
;;  ((free "lilv_scale_points_free"))
;;  (class-slot-set!  <scale-points>  'free free))

