(define-module (lilv oop collection)
  #:use-module (lilv oop base)
  #:use-module (srfi srfi-1)
  #:use-module (oop goops)
  #:export (<collection>
            fold
            iterate
            size
            ->list
            define-lilv-collection))

(define-class <collection> (<lilv>))


(define-generic iterate)
(define-generic ->list)
(define-generic size)

(define-method (fold (kons <procedure>) (collection <collection>) knil)
  (define iter (iterate collection))
  (let lp ((knil knil))
    (let [(item (iter))]
      (if item
          (lp (kons item knil))
          knil))))


(define-method (->list (collection <collection>))
  (reverse! (fold cons collection '())))

(define (make-lilv-iterator %get %next %ended? %it %coll)
  (lambda ()
    (if (zero? (%ended? %coll %it))
        (let [(item (%get %coll %it))]
         (set! %it (%next %coll %it))
         item)
        #f)))


(define-syntax define-lilv-collection
  (lambda (x)
    (syntax-case x (on next start get ended? size)
      [(_ collection-class
          (on      target-class)
          (next    next-binding)
          (start   start-binding)
          (get     get-binding)
          (ended?  ended-binding)
          (size    size-binding))
       #'(define collection-class
           (lilv-bind
            [(%start  ('* start-binding '*))
             (%size   (ffi:int  size-binding  '*))
             (%get    ('* get-binding   '* '*))
             (%next   ('* next-binding  '* '*))
             (%ended? (ffi:int ended-binding '* '*))]
            
            (let [(k (class (<collection>) #:name 'collection-class))
                  (get (lambda (%coll %it)
                         (make target-class #:pointer (%get %coll %it))))]
              (add-method!
               iterate
               (method ((c k))
                       (let* [(%coll (->pointer c))
                              (%it (%start %coll))]
                         (make-lilv-iterator get %next %ended? %it %coll))))

              (add-method!
               size
               (method ((c k))
                       (%size (->pointer c))))
              k)))])))
