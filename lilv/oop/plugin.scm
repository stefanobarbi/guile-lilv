(define-module (lilv oop plugin)
  #:use-module ((system foreign) #:prefix ffi:)
  #:use-module (lilv oop base)
  #:use-module (lilv oop world)
  #:use-module (lilv oop world model)
  #:use-module (lilv oop node)
  #:use-module (lilv oop collection)
  #:use-module (oop goops)
  #:export (<plugin>
            world-plugins
            world-plugin-classes
            plugin-class
            plugin))

(define-class <plugin> (<lilv>))
(define-class <plugin-class> (<lilv>))

(define-lilv-collection <plugins>
  (on <plugin>)
  (next    "lilv_plugins_next")
  (start   "lilv_plugins_begin")
  (get     "lilv_plugins_get")
  (ended?  "lilv_plugins_is_end")
  (size    "lilv_plugins_size"))

(define-lilv-collection <plugin-classes>
  (on <plugin-class>)
  (next    "lilv_plugin_classes_next")
  (start   "lilv_plugin_classes_begin")
  (get     "lilv_plugin_classes_get")
  (ended?  "lilv_plugin_classes_is_end")
  (size    "lilv_plugin_classes_size"))

(define-lilv-method (uri (p <plugin>))
  ((uri ('* "lilv_plugin_get_uri" '*)))
  (make <node>  #:pointer (uri (->pointer p)) #:collect? #f))

(define-lilv-method (name (p <plugin>))
  ((name ('* "lilv_plugin_get_name" '*)))
  (make <node>  #:pointer (name (->pointer p))))


(define-lilv (world-plugins)
  ((all ('* "lilv_world_get_all_plugins" '*)))
  (make <plugins> #:pointer (all (->pointer (the-world))) #:collect? #f))

;;; get a plugin by uri
(define-lilv-method (plugin (uri <node-uri>))
  ((get-plugin ('* "lilv_plugins_get_by_uri" '* '*)))
  (let [(ptr (get-plugin (->pointer (world-plugins))
                         (->pointer uri)))]
    (and (not (ffi:null-pointer? ptr))
       (make <plugin> #:pointer ptr #:collect? #f))))


(define-method (plugin (uri <string>))
  (plugin (node <node-uri> uri)))

(define-lilv (world-plugin-classes)
  ((all ('* "lilv_world_get_plugin_classes" '*)))
  (make <plugin-classes> #:pointer (all (->pointer (the-world))) #:collect? #f))

(define-lilv-method (uri ( c <plugin-class>))
  ((get-uri ('* "lilv_plugin_class_get_uri" '*)))
  (make <node-uri> #:pointer (get-uri (->pointer c )) #:collect? #f))


(define-lilv-method (plugin-class (p <plugin>))
  ((get-class ('* "lilv_plugin_get_class" '*)))
  (make <plugin-class> #:pointer (get-class (->pointer p))))

