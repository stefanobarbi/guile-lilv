(define-module (lilv oop world)
  #:use-module (lilv oop base )
  #:use-module (oop goops)
  #:use-module ((system foreign) #:prefix ffi:)
  #:export (<world>
            the-world
            world-load-all))


(lilv-bind
 ((free-world "lilv_world_free"))
 (define-class <world> (<lilv>)
   (free #:init-value free-world #:allocation #:class)))


(define-lilv (make-world)
  ((%make-world ('* "lilv_world_new")))
  (make <world> #:pointer (%make-world)))


(define-lilv (world-load-all)
  ((%load-all (ffi:void "lilv_world_load_all" '*)))
  (%load-all (->pointer (the-world))))


(define the-world (make-parameter (make-world)))




